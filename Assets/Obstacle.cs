using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float movespeed = 4f;

    public float minSpinSpeed = 180f;
    public float maxSpinSpeed = 360f;

    private float spinSpeed = 180f;
    private float spinSign = 1f;

    private Rigidbody2D rgbd;

    public GameObject obstacleParticles;

    public SuperManager superManager;

    private bool isDestroyed = false;

    private void Start()
    {
        superManager = SuperManager.instance;

        // random spin sign
        spinSign = Mathf.Sign(Random.Range(-1f, 1f));

        // random spin speed
        spinSpeed = Random.Range(minSpinSpeed, maxSpinSpeed);

        // speed multiplier if on last level
        float speedMultiplier = superManager.levelIndex >= 3 ? 1.2f : 1f;

        // set angular velocity
        rgbd = GetComponent<Rigidbody2D>();
        rgbd.angularVelocity = spinSpeed * spinSign * speedMultiplier;
        
        // set velocity
        rgbd.velocity = Vector3.up * movespeed * speedMultiplier;
    }

    private void DestroyObstacle()
    {
        // spawn particles
        GameObject particles = Instantiate(obstacleParticles, transform.position, Quaternion.identity);
        particles.transform.localScale = transform.localScale;
        Destroy(particles, 2f);
        
        // destroy self
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {
        if ((superManager.isTransitionning || superManager.isFinished) && !isDestroyed)
        {
            DestroyObstacle();
            isDestroyed = true;
        }
    }
}
