using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [Header("Params")]
    public float maxVolume = 0.5f;

    public float fadeDelay = 1 / 60f;
    public float fadeStep = 1 / 30f;

    [Header("Audio Sources")]
    public AudioSource beatsSource;
    public AudioSource bassSource;
    public AudioSource twoSource;
    public AudioSource threeSource;
    public AudioSource fourSource;
    public AudioSource humSource;

    private void Start()
    {
        beatsSource.volume = maxVolume;
        bassSource.volume = 0f;
        twoSource.volume = 0f;
        threeSource.volume = 0f;
        fourSource.volume = 0f;
        humSource.volume = 0f;

        // play everything
        beatsSource.Play();
        bassSource.Play();
        twoSource.Play();
        threeSource.Play();
        fourSource.Play();
        humSource.Play();
    }

    public void SwitchTracks(Track track)
    {
        switch (track)
        {
            case Track.BEATS:
                StartCoroutine(SwitchToTrack(beatsSource, bassSource, twoSource, threeSource, fourSource, humSource));
                break;

            case Track.BASS:
                StartCoroutine(SwitchToTrack(bassSource, beatsSource, twoSource, threeSource, fourSource, humSource));
                break;

            case Track.TWO:
                StartCoroutine(SwitchToTrack(twoSource, beatsSource, bassSource, threeSource, fourSource, humSource));
                break;

            case Track.THREE:
                StartCoroutine(SwitchToTrack(threeSource, beatsSource, bassSource, twoSource, fourSource, humSource));
                break;

            case Track.FOUR:
                StartCoroutine(SwitchToTrack(fourSource, beatsSource, bassSource, twoSource, threeSource, humSource));
                break;

            case Track.HUM:
                StartCoroutine(SwitchToTrack(humSource, beatsSource, bassSource, twoSource, threeSource, fourSource));
                break;
        }
    }

    private IEnumerator SwitchToTrack(AudioSource desiredSource, AudioSource otherA, AudioSource otherB, AudioSource otherC, AudioSource otherD, AudioSource otherE)
    {
        while (desiredSource.volume < maxVolume)
        {
            desiredSource.volume += fadeStep;
            otherA.volume -= fadeStep;
            otherB.volume -= fadeStep;
            otherC.volume -= fadeStep;
            otherD.volume -= fadeStep;
            otherD.volume -= fadeStep;

            yield return new WaitForSecondsRealtime(fadeDelay);
        }

        desiredSource.volume = 0.5f;
        otherA.volume = 0f;
        otherB.volume = 0f;
        otherC.volume = 0f;
        otherD.volume = 0f;
        otherE.volume = 0f;
    }

    public enum Track
    {
        BEATS,
        BASS,
        TWO,
        THREE,
        FOUR,
        HUM
    }
}
