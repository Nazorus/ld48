using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Controls")]
    public float movespeed = 16f;
    public float dashImpulse = 32f;

    [Header("Visuals")]
    public GameObject deathParticles;

    public PlayerShadow shadowPrefab;
    public float shadowDelay = 0.2f;

    public GameObject dashParticlesRight;
    public GameObject dashParticlesLeft;

    [Header("Sprites")]
    public Sprite defaultStance;
    public Sprite shootDownStance;
    public Sprite shootUpStance;
    public Sprite dashStance;

    private Stance stance = Stance.DEFAULT;

    private SpriteRenderer spriteRenderer;

    public float defaultStanceCooldown = 0.5f;
    private float defaultStanceTimer = 0f;

    [Header("Audio")]
    public AudioSource mainAudioSource;
    public AudioSource voiceAudioSource;

    public AudioClip shootClip;
    public List<AudioClip> voiceShootClips;
    public List<AudioClip> dashClips;

    [Header("Other")]
    public ShakyText gameOverText;

    public LineRenderer aimLaser;
    public ShootLaser shootLaser;
    public float aimDistance = 4f;
    public float shootDistance = 2f;

    private Rigidbody2D rgbd;

    private Camera mainCamera;

    // controller aim
    private bool isControllerAim = false;

    // raycast ignored layers
    private LayerMask raycastIgnoreMask;

    private SuperManager superManager;

    void Start()
    {
        // get components
        spriteRenderer = GetComponent<SpriteRenderer>();
        rgbd = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;

        superManager = SuperManager.instance;

        // raycast ignored layers
        raycastIgnoreMask = ~(1 << LayerMask.NameToLayer("Player") | 1 << LayerMask.NameToLayer("Level"));

        // start spawning shadows
        StartSpawningShadows();
    }

    void Update()
    {
        // movement
        float horizontalMove = Input.GetAxis("Horizontal");
        float verticalMove = Input.GetAxis("Vertical");

        // calculate and apply
        Vector2 moveVector = (Vector2.right * horizontalMove + Vector2.up * verticalMove).normalized; ;
        rgbd.AddForce(moveVector * movespeed * Time.deltaTime);

        // flip player sprite
        if (stance == Stance.DEFAULT && horizontalMove != 0)
        {
            Vector3 playerScale = transform.localScale;
            playerScale.x = Mathf.Sign(horizontalMove);
            transform.localScale = playerScale;
        }

        // calculate aimDirection
        Vector3 aimDirection;
        if (isControllerAim)
        {
            aimDirection = new Vector3(Input.GetAxis("AimX"), -Input.GetAxis("AimY"), 0f).normalized;
        }
        else
        {
            // get cursor position
            Vector3 cursorPosition = Input.mousePosition;
            cursorPosition.z = 10f;
            cursorPosition = mainCamera.ScreenToWorldPoint(cursorPosition);

            // raycast using aim direction
            aimDirection = (cursorPosition - transform.position).normalized;
        }
        RaycastHit2D hit = Physics2D.Raycast(transform.position, aimDirection, 100f, raycastIgnoreMask);

        // show aim laser
        aimLaser.SetPosition(0, transform.position);
        if (aimLaser != null && hit.collider != null && Vector2.Distance(hit.point, transform.position) < aimDistance)
        {
            // case of obstacle or level
            aimLaser.SetPosition(1, hit.point);
        }
        else
        {
            aimLaser.SetPosition(1, transform.position + aimDirection * aimDistance);
        }

        // fire
        if (Input.GetButtonDown("Fire1"))
        {
            // always update start of shoot laser
            shootLaser.SetStartPosition(transform.position + aimDirection * shootDistance);

            // check if raycast hit something
            if (hit.collider != null)
            {
                Pickup pickup = hit.collider.GetComponent<Pickup>();
                if (pickup != null)
                {
                    // hit pickup, et and shoot "through" it
                    pickup.GetPickup();
                    shootLaser.SetEndPosition(transform.position + aimDirection * 100f);
                }
                else
                {
                    // hit obstacle
                    shootLaser.SetEndPosition(hit.point);
                }
            }
            else
            {
                // hit nothing
                shootLaser.SetEndPosition(transform.position + aimDirection * 100f);
            }

            // play laser animation
            shootLaser.Shoot();

            // play clip
            mainAudioSource.PlayOneShot(shootClip);
            voiceAudioSource.PlayOneShot(voiceShootClips[Random.Range(0, voiceShootClips.Count)]);

            // shoot stance
            if (aimDirection.y >= 0)
            {
                SwitchStance(Stance.SHOOT_UP);
            }
            else
            {
                SwitchStance(Stance.SHOOT_DOWN);
            }

            // face shoot direction
            Vector3 playerScale = transform.localScale;
            playerScale.x = Mathf.Sign(aimDirection.x);
            transform.localScale = playerScale;
        }

        // dash
        if (Input.GetButtonDown("Fire2"))
        {
            // dash twards aim direction
            rgbd.AddForce(aimDirection * dashImpulse, ForceMode2D.Impulse);

            // dash stance
            SwitchStance(Stance.DASH);

            // face dash direction
            Vector3 playerScale = transform.localScale;
            playerScale.x = Mathf.Sign(aimDirection.x);
            transform.localScale = playerScale;

            // show particles
            if (aimDirection.x >= 0)
            {
                Destroy(Instantiate(dashParticlesRight, transform.position, Quaternion.identity), 2f);
            }
            else
            {
                Destroy(Instantiate(dashParticlesLeft, transform.position, Quaternion.identity), 2f);
            }

            // play sound
            voiceAudioSource.PlayOneShot(dashClips[Random.Range(0, dashClips.Count)]);
        }

        // default stance
        if (stance != Stance.DEFAULT && defaultStanceTimer <= 0f)
        {
            SwitchStance(Stance.DEFAULT);
        }
        else
        {
            defaultStanceTimer -= Time.deltaTime;
        }
    }

    private void SwitchStance(Stance stance)
    {
        // set stance
        this.stance = stance;

        // set timer
        defaultStanceTimer = defaultStanceCooldown;

        // set sprite
        switch (stance)
        {
            case Stance.DEFAULT:
                spriteRenderer.sprite = defaultStance;
                break;

            case Stance.SHOOT_DOWN:
                spriteRenderer.sprite = shootDownStance;
                break;

            case Stance.SHOOT_UP:
                spriteRenderer.sprite = shootUpStance;
                break;

            case Stance.DASH:
                spriteRenderer.sprite = dashStance;
                break;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // die if hit by obstacle
        if (collision.gameObject.GetComponent<Obstacle>() != null)
        {
            // spawn particles
            Instantiate(deathParticles, transform.position, Quaternion.identity);

            // show game over text
            //gameOverText.gameObject.SetActive(true);
            //gameOverText.UpdateAndSkake("OOF");

            // shake camera hard
            mainCamera.GetComponent<CameraShake>().Shake(4f);

            // notify manager
            superManager.SetDead();

            // "die"
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }

    public void StartSpawningShadows()
    {
        StartCoroutine(SpawnShadows());
    }

    private IEnumerator SpawnShadows()
    {
        while (isActiveAndEnabled)
        {
            int colorIndex = superManager.IsRunning() ? superManager.levelIndex : -1;
            Instantiate(shadowPrefab, transform.position, Quaternion.identity).Init(this, spriteRenderer.sprite, colorIndex);
            yield return new WaitForSeconds(shadowDelay);
        }
    }

    private void OnDisable()
    {
        if (aimLaser != null) aimLaser.gameObject.SetActive(false);
        if (shootLaser != null) shootLaser.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        aimLaser.gameObject.SetActive(true);
        shootLaser.gameObject.SetActive(true);
    }

    private enum Stance
    {
        DEFAULT,
        SHOOT_DOWN,
        SHOOT_UP,
        DASH
    }
}
