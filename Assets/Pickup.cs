using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public float movespeed = 4f;

    private Rigidbody2D rgbd;

    public GameObject particles;
    public GameObject transitionParticles;

    private bool isDestroyed = false;

    private SuperManager superManager;

    void Start()
    {
        superManager = SuperManager.instance;

        // set velocity
        rgbd = GetComponent<Rigidbody2D>();
        rgbd.velocity = Vector3.up * movespeed;
    }

    public void GetPickup()
    {
        // increment scrore
        SuperManager.instance.IncrementScore();

        // show particles
        Destroy(Instantiate(particles, transform.position, Quaternion.identity), 2f);

        // destroy self
        Destroy(gameObject);
    }


    private void FixedUpdate()
    {
        if ((superManager.isTransitionning || superManager.isFinished) && !isDestroyed)
        {
            isDestroyed = true;

            // show particles
            Destroy(Instantiate(transitionParticles, transform.position, Quaternion.identity), 2f);

            // destroy self
            Destroy(gameObject);
        }
    }
}
