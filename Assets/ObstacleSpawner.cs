using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [Header("Obstacle Prefabs")]
    public Obstacle obstaclePrefab;
    public Obstacle triangleObstaclePrefab;
    public Obstacle longObstaclePrefab;

    public Pickup pickupPrefab;

    public float minSpawnDelay = 1f;
    public float maxSpawnDelay = 2f;

    public float obstacleLifespan = 10f;

    public float maxSpawnOffset = 8f;

    public float minObstacleScale = 0.5f;
    public float maxObstacleScale = 4f;

    public float triangleObstacleScale = 0.5f;

    public float minLongObstacleScale = 0.75f;
    public float maxLongObstacleScale = 2f;

    private SuperManager superManager;

    void Start()
    {
        superManager = SuperManager.instance;

        StartCoroutine(SpwawnObstacles());
        StartCoroutine(SpwawnTriangles());
        StartCoroutine(SpwawnLongObstacles());
        StartCoroutine(SpwawnPickups());
    }

    private IEnumerator SpwawnObstacles()
    {
        while (isActiveAndEnabled)
        {
            float delay = Random.Range(minSpawnDelay, maxSpawnDelay) * GetDelayMultiplier();
            yield return new WaitForSeconds(delay);

            // randomized spawn position
            Vector3 spawnPosition = transform.position + Vector3.right * Random.Range(-maxSpawnOffset, maxSpawnOffset);

            // spawn normal obstacle
            Obstacle obstacle = Instantiate(obstaclePrefab, spawnPosition, Quaternion.identity);
            obstacle.transform.localScale = Vector3.one * Random.Range(minObstacleScale, maxObstacleScale);

            // destroy obstacle after delay
            Destroy(obstacle.gameObject, obstacleLifespan);
        }
    }

    private IEnumerator SpwawnTriangles()
    {
        while (isActiveAndEnabled)
        {
            float delay = Random.Range(minSpawnDelay, maxSpawnDelay);
            yield return new WaitForSeconds(delay);

            // don't spawn if not above level index 1
            if (superManager.levelIndex < 1) continue;

            // randomized spawn position
            Vector3 spawnPosition = transform.position + Vector3.right * Random.Range(-maxSpawnOffset, maxSpawnOffset);

            // spawn triangle
            Obstacle triangle = Instantiate(triangleObstaclePrefab, spawnPosition, Quaternion.identity);
            triangle.transform.localScale = Vector3.one * triangleObstacleScale;

            // destroy obstacle after delay
            Destroy(triangle.gameObject, obstacleLifespan);
        }
    }

    private IEnumerator SpwawnLongObstacles()
    {
        while (isActiveAndEnabled)
        {
            float delay = Random.Range(minSpawnDelay, maxSpawnDelay) * GetDelayMultiplier();
            yield return new WaitForSeconds(delay);

            // don't spawn if not above level index 1
            if (superManager.levelIndex < 2) continue;

            // randomized spawn position
            Vector3 spawnPosition = transform.position + Vector3.right * Random.Range(-maxSpawnOffset, maxSpawnOffset);

            // spawn triangle
            Obstacle obstacle = Instantiate(longObstaclePrefab, spawnPosition, Quaternion.identity);
            obstacle.transform.localScale = Vector3.one * triangleObstacleScale;

            // destroy obstacle after delay
            Destroy(obstacle.gameObject, obstacleLifespan);
        }
    }

    private IEnumerator SpwawnPickups()
    {
        while (isActiveAndEnabled)
        {
            float delay = Random.Range(minSpawnDelay, maxSpawnDelay);
            yield return new WaitForSeconds(delay);

            // randomized spawn position
            Vector3 spawnPosition = transform.position + Vector3.right * Random.Range(-maxSpawnOffset, maxSpawnOffset);

            // spawn obstacle
            Pickup pickup = Instantiate(pickupPrefab, spawnPosition, Quaternion.identity);

            // destroy obstacle after delay
            Destroy(pickup.gameObject, obstacleLifespan);
        }
    }

    private float GetDelayMultiplier()
    {
        if (superManager.levelIndex == 0) return 1f;
        if (superManager.levelIndex == 1) return 1f;
        if (superManager.levelIndex == 2) return 1.5f;
        if (superManager.levelIndex == 3) return 1f;

        return 1f;
    }
}
