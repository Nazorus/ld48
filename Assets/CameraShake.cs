using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeMagnitude = 2f;
    public float shakeRecover = 10f;

    private Camera cameraComponent;
    private Vector3 defaultPosition;

    void Start()
    {
        cameraComponent = GetComponent<Camera>();
        defaultPosition = transform.position;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, defaultPosition, Time.deltaTime * shakeRecover);
    }

    public void Shake(float shakeIntensity = 1f)
    {
        Vector3 shakeOffset = Random.insideUnitCircle.normalized * shakeMagnitude * shakeIntensity;
        transform.position += shakeOffset;
    }
}
