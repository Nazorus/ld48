using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthMeter : MonoBehaviour
{
    public Transform meterCursor;

    public float startHeight = 0f;
    public float endHeight = -600f;

    private SuperManager superManager;

    void Start()
    {
        superManager = SuperManager.instance;
    }

    void Update()
    {
        Vector3 cursorPosition = meterCursor.localPosition;
        //cursorPosition.y = Mathf.Lerp(startHeight, endHeight, superManager.depthValue / superManager.maxDepth);
        meterCursor.localPosition = cursorPosition;
    }
}
