using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallParticles : MonoBehaviour
{
    public List<Color> particleColors;

    private ParticleSystem particlesParent;
    private ParticleSystem.MainModule particles;
    private ParticleSystem.EmissionModule particlesEmission;

    public float defaultParticleSpeed = 1f;
    public float transitionParticleSpeed = 4f;

    public float defaultParticleEmission = 20f;
    public float transitionParticleEmission = 80f;

    public float speedRecover = 4f;

    private void Awake()
    {
        particlesParent = GetComponent<ParticleSystem>();
        particles = GetComponent<ParticleSystem>().main;
        particlesEmission = particlesParent.emission;
    }

    public void SwitchColor(int colorIndex)
    {
        // set new color
        particles.startColor = particleColors[Mathf.Min(colorIndex, particleColors.Count - 1)];

        // faster particles
        particles.startSpeedMultiplier = transitionParticleSpeed;

        // increase emission
        particlesEmission.rateOverTime = transitionParticleEmission;

        // clear existing particles
        particlesParent.Clear();
    }

    void Update()
    {
        // restore normal speed and emission
        particles.startSpeedMultiplier = Mathf.Lerp(particles.startSpeedMultiplier, defaultParticleSpeed, Time.deltaTime * speedRecover);
        particlesEmission.rateOverTime = Mathf.Lerp(particlesEmission.rateOverTime.constant, defaultParticleEmission, Time.deltaTime * speedRecover);
    }
}
