using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShakyText : MonoBehaviour
{
    public float shakeMagnitude = 2f;
    public float shakeRecover = 10f;

    private TextMeshProUGUI text;

    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * shakeRecover);
    }

    public void UpdateAndSkake(string text, float shakeFactor = 1f)
    {
        if (this.text == null)
        {
            this.text = GetComponent<TextMeshProUGUI>();
        }

        this.text.text = text;
        transform.localScale = Vector3.one * shakeMagnitude * shakeFactor;
    }
}
