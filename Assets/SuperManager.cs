using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SuperManager : MonoBehaviour
{
    // singleton
    public static SuperManager instance { get; private set; }

    public float depthSpeed = 16f;
    public float depthValue = 0f;
    private float previousIntDepthValue = 0;

    public ShakyText depthText;
    public ShakyText scoreText;

    public float transitionDepthFactor = 4f;
    public float transitionShakeFactor = 2f;

    // states
    private bool isTutorial = true;
    public bool isFinished = false;
    private bool isDead = false;

    [Header("Levels")]
    public int levelIndex = 0;
    public int maxLevelIndex = 4;
    
    public int scorePerLevel = 20;
    private int score = 0;

    public Player player;
    private Vector3 defaultPlayerPosition;

    public bool isTransitionning = false;

    public float transitionDuration = 1f;

    [Header("Sky")]
    public Color skyTint;
    private float defaultSkyHue;

    public Color darkSkyTint = Color.white;

    public List<Color> skyTints;

    public float skyExposure = 1f;
    private float defaultSkyExposure;
    public float skyExposureRecover = 8f;
    public float skyExposureMagnitude = 0.1f;

    public float hueMagnitude = 0.5f;
    public float hueSpeed = 10f;

    [Header("Particles")]
    public FallParticles fallParticles;

    [Header("Tutorial")]
    public TutorialText tutorialText;
    public Pickup tutorialOrb;
    private TutorialState tutorialState = TutorialState.MOVE;

    private float moveTime = 0.5f;
    private float dashCount = 2;
    private float doneTime = 2f;

    [Header("Other")]
    public ObstacleSpawner obstacleSpawner;
    public CameraShake cameraShake;

    public GameObject respawnParticles;

    public MusicPlayer musicPlayer;

    [Header("End Sequence")]
    public TutorialText endTitle;
    public TutorialText endThanks;
    public TutorialText endRestart;

    void Awake()
    {
        // unity singleton
        if (instance == null)
        {
            // init singleton
            instance = this;
        }
        else
        {
            // do not replace existing singleton, self-destruct
            Destroy(gameObject);
        }
    }

    void Start()
    {
        // set sky tint
        RenderSettings.skybox.SetColor("_SkyTint", darkSkyTint);

        // set default exposure
        defaultSkyExposure = skyExposure;

        // show tutorial text
        tutorialText.gameObject.SetActive(true);
        tutorialText.Init("move with arrows or WASD");

        // player position
        defaultPlayerPosition = player.transform.position;
    }

    void Update()
    {
        // tutorial
        if (isTutorial)
        {
            HandleTutorial();
        }

        // restart when pressing R
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // update depth value if game isn't finished
        if (!isFinished && !isTutorial)
        {
            // increment depth
            float depthMultiplier = isTransitionning ? transitionDepthFactor : 1f;
            depthValue += Time.deltaTime * depthSpeed * depthMultiplier;

            // only update text when int value has changed
            int intDepthValue = Mathf.FloorToInt(depthValue);
            if (intDepthValue != previousIntDepthValue)
            {
                depthText.UpdateAndSkake(Mathf.FloorToInt(depthValue).ToString(), isTransitionning ? transitionShakeFactor : 1f);
            }
            previousIntDepthValue = intDepthValue;
        }

        // get current sky tint
        if (isFinished || isTutorial)
        {
            RenderSettings.skybox.SetColor("_SkyTint", darkSkyTint);
        }
        else
        {
            Color skyTint = skyTints[Mathf.Min(levelIndex, skyTints.Count - 1)];

            // get original color as HSV
            Color.RGBToHSV(skyTint, out float h, out float s, out float v);

            // modulate hue
            h += Mathf.Sin(Time.time * hueSpeed) * hueMagnitude;

            // apply
            RenderSettings.skybox.SetColor("_SkyTint", Color.HSVToRGB(h, s, v));
        }

        // restore sky exposure
        RenderSettings.skybox.SetFloat("_Exposure", skyExposure);
        skyExposure = Mathf.Lerp(skyExposure, defaultSkyExposure, Time.deltaTime * skyExposureRecover);
    }

    private void UpdateScoreText()
    {
        // update text
        if (isFinished)
        {
            scoreText.UpdateAndSkake(scorePerLevel.ToString() + "/" + scorePerLevel.ToString());
        }
        else
        {
            scoreText.UpdateAndSkake(score.ToString() + "/" + scorePerLevel.ToString());
        }
    }

    public void IncrementScore()
    {
        // ignore during tutorial
        if (isTutorial) return;

        // update score
        score++;
        UpdateScoreText();

        // flash sky
        FlashSky();

        // shake camera
        cameraShake.Shake();

        // update level index
        int previousIndex = levelIndex;
        if (score >= scorePerLevel)
        {
            levelIndex++;
            score = 0;
            UpdateScoreText();
        }
        
        // check if game is finished
        if (levelIndex >= maxLevelIndex)
        {
            SetFinished();
            UpdateScoreText();
        }

        // update particles
        if (previousIndex != levelIndex)
        {
            StartCoroutine(TriggerLevelTransition(true));
        }
    }

    private IEnumerator TriggerLevelTransition(bool switchTracks = false)
    {
        // switch particle colors
        fallParticles.SwitchColor(levelIndex);
        
        // switch track
        if (switchTracks)
        {
            switch (levelIndex)
            {
                case 0:
                    musicPlayer.SwitchTracks(MusicPlayer.Track.BASS);
                    break;
                case 1:
                    musicPlayer.SwitchTracks(MusicPlayer.Track.TWO);
                    break;
                case 2:
                    musicPlayer.SwitchTracks(MusicPlayer.Track.THREE);
                    break;
                case 3:
                    musicPlayer.SwitchTracks(MusicPlayer.Track.FOUR);
                    break;
            }
        }

        // other stuff
        isTransitionning = true;
        yield return new WaitForSeconds(transitionDuration);
        isTransitionning = false;
    }

    private void SetFinished()
    {
        isFinished = true;

        obstacleSpawner.gameObject.SetActive(false);
        fallParticles.gameObject.SetActive(false);

        // switch to beats
        musicPlayer.SwitchTracks(MusicPlayer.Track.HUM);

        // show end sequence
        StartCoroutine(ShowEnd());
    }

    private IEnumerator ShowEnd()
    {
        yield return new WaitForSeconds(2f);
        endTitle.gameObject.SetActive(true);
        endTitle.Init("CAPTAIN  ZAPDASH");

        yield return new WaitForSeconds(2f);
        endThanks.gameObject.SetActive(true);
        endThanks.Init("thanks for playing");

        yield return new WaitForSeconds(2f);
        endRestart.gameObject.SetActive(true);
        endRestart.Init("press R to restart");
    }

    public void SetDead()
    {
        isDead = true;

        // reset score
        score = 0;
        UpdateScoreText();

        // flash
        FlashSky();

        // replay level transition
        StartCoroutine(TriggerLevelTransition());

        // respawn
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(2f);
        isDead = false;

        // move player to default position
        player.transform.position = defaultPlayerPosition;

        // show respawn particles
        Destroy(Instantiate(respawnParticles, defaultPlayerPosition, Quaternion.identity), 2f);

        // enable player
        player.gameObject.SetActive(true);
        player.StartSpawningShadows();
    }

    public bool IsRunning()
    {
        return !isTutorial && !isFinished;
    }

    private void HandleTutorial()
    {
        switch (tutorialState)
        {
            case TutorialState.MOVE:
                if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
                {
                    // count move time
                    moveTime -= Time.deltaTime;
                }
                if (moveTime <= 0f)
                {
                    // move to next phase
                    tutorialState = TutorialState.DASH;
                    tutorialText.Init("dash with right click");
                    return;
                }
                break;

            case TutorialState.DASH:
                if (Input.GetButtonDown("Fire2"))
                {
                    dashCount--;
                }
                if (dashCount <= 0)
                {
                    // next phase
                    tutorialState = TutorialState.ZAP;
                    tutorialText.Init("zap orbs with left click");

                    // enable tutorial orb
                    tutorialOrb.gameObject.SetActive(true);
                    return;
                }
                break;

            case TutorialState.ZAP:
                if (tutorialOrb == null)
                {
                    tutorialState = TutorialState.DONE;
                    tutorialText.Init("zap the orbs!");

                    return;
                }
                break;

            case TutorialState.DONE:
                doneTime -= Time.deltaTime;
                if (doneTime <= 0f)
                {
                    tutorialText.gameObject.SetActive(false);
                    isTutorial = false;

                    FlashSky();

                    // enable spawner and particles
                    obstacleSpawner.gameObject.SetActive(true);
                    fallParticles.gameObject.SetActive(true);

                    // update score text
                    UpdateScoreText();

                    // switch tracks
                    musicPlayer.SwitchTracks(MusicPlayer.Track.BASS);
                    return;
                }
                break;
        }
    }

    private void FlashSky()
    {
        // flash sky
        skyExposure += skyExposureMagnitude;
    }

    private enum TutorialState
    {
        MOVE,
        DASH,
        ZAP,
        DONE
    }
}
