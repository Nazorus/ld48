using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootLaser : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public float shootWidth = 1f;

    public float shrinkSpeed = 16f;

    void Start()
    {
        lineRenderer.widthMultiplier = 0f;
    }

    void Update()
    {
        lineRenderer.widthMultiplier = Mathf.Lerp(lineRenderer.widthMultiplier, 0f, Time.deltaTime * shrinkSpeed);
    }

    public void Shoot()
    {
        lineRenderer.widthMultiplier = shootWidth;
    }

    public void SetStartPosition(Vector3 position)
    {
        lineRenderer.SetPosition(0, position);
    }

    public void SetEndPosition(Vector3 position)
    {
        lineRenderer.SetPosition(1, position);
    }
}
