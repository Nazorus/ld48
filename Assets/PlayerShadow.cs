using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShadow : MonoBehaviour
{
    public float shrinkSpeed = 4f;
    public float fadeSpeed = 4f;

    public float lifespan = 1f;

    public float movespeed = 4f;

    public Color neutralColor = Color.white;

    public List<Color> colors;

    private SpriteRenderer spriteRenderer;

    private Player player;

    public void Init(Player player, Sprite sprite, int colorIndex)
    {
        this.player = player;
        transform.localScale = player.transform.localScale;

        // compenents
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;

        // set color
        if (colorIndex < 0)
        {
            spriteRenderer.color = neutralColor;
        }
        else
        {
            spriteRenderer.color = colors[Mathf.Min(colorIndex, colors.Count - 1)];
        }
    }

    void Start()
    {
        
        // destroy self after lifespan
        Destroy(gameObject, lifespan);
    }

    void Update()
    {
        // fade
        Color color = spriteRenderer.color;
        color.a = Mathf.Lerp(color.a, 0f, Time.deltaTime * fadeSpeed);
        spriteRenderer.color = color;

        // reproduce player scale and use same sprite
        if (player != null)
        {
            transform.localScale = player.transform.localScale;
        }

        // shrink
        //transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.deltaTime * shrinkSpeed);

        // move up
        transform.position += Vector3.up * movespeed * Time.deltaTime;
    }
}
