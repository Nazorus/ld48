using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TutorialText : MonoBehaviour
{
    public float movespeed = 8f;

    public float shakeMagnitude = 4f;
    public float shakeRecover = 8f;

    public float startY = -800f;
    public float endY = 0f;

    private TextMeshProUGUI text;

    public void Init(string text)
    {
        // move down
        transform.localPosition = Vector3.zero + Vector3.up * startY;

        // shake
        transform.localScale = Vector3.one * shakeMagnitude;

        // set text
        this.text = GetComponent<TextMeshProUGUI>();
        this.text.text = text;
    }

    void Update()
    {
        // restore scale
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * shakeRecover);

        // move to end Y
        transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero + Vector3.up * endY, Time.deltaTime * movespeed);
    }
}
